/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
///////////////////////////LABIRINTO/////////////////////////////////
/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////

const map = [
    "WWWWWWWWWWWWWWWWWWWWW",
    "W   W     W     W W W",
    "W W W WWW WWWWW W W W",
    "W W W   W     W W   W",
    "W WWWWWWW W WWW W W W",
    "W         W     W W W",
    "W WWW WWWWW WWWWW W W",
    "W W   W   W W     W W",
    "W WWWWW W W W WWW W F",
    "R     W W W W W W WWW",
    "WWWWW W W W W W W W W",
    "W     W W W   W W W W",
    "W WWWWWWW WWWWW W W W",
    "W       W       W   W",
    "WWWWWWWWWWWWWWWWWWWWW",
];

// map[1][4]

let mapSplit00 = map[0].split('');
let mapSplit01 = map[1].split('');
let mapSplit02 = map[2].split('');
let mapSplit03 = map[3].split('');
let mapSplit04 = map[4].split('');
let mapSplit05 = map[5].split('');
let mapSplit06 = map[6].split('');
let mapSplit07 = map[7].split('');
let mapSplit08 = map[8].split('');
let mapSplit09 = map[9].split('');
let mapSplit10 = map[10].split('');
let mapSplit11 = map[11].split('');
let mapSplit12 = map[12].split('');
let mapSplit13 = map[13].split('');
let mapSplit14 = map[14].split('');


const mapSplitComplete = mapSplit00.concat(mapSplit01, mapSplit02, mapSplit03, 
                        mapSplit04, mapSplit05, mapSplit06, mapSplit07, mapSplit08, mapSplit09, mapSplit10, 
                        mapSplit11, mapSplit12, mapSplit13, mapSplit14);

let lego = '';
let distanceX = '';
let distanceY = '';
let wall_X = [];
let wall_Y = [];
let labirinth = document.getElementById("path");

function createLabirinth() {
    for(mapSplitComplete[ a = 0 ]; a <= mapSplitComplete.length; a++) {

                //....CALCULLAR EIXO X....//
        if(a <= 20) {
            distanceX = 50 * a;
        }
        else if(a > 20 && a <= 41) {
            distanceX = (50 * (a-21));
        }
        else if(a > 41 && a <= 62) {
            distanceX = (50 * (a-42));
        }
        else if(a > 62 && a <= 83) {
            distanceX = (50 * (a - 63));
        }
        else if(a > 83 && a <= 104) {
            distanceX = (50 * (a - 84));
        }
        else if(a > 104 && a <= 125) {
            distanceX = (50 * (a - 105));
        }
        else if(a > 125 && a <= 146) {
            distanceX = (50 * (a - 126));
        }
        else if(a > 146 && a <= 167) {
            distanceX = (50 * (a - 147));
        }
        else if(a > 167 && a <= 188) {
            distanceX = (50 * (a - 168));
        }
        else if(a > 188 && a <= 209) {
            distanceX = (50 * (a - 189));
        }
        else if(a > 209 && a <= 230) {
            distanceX = (50 * (a - 210));
        }
        else if(a > 230 && a <= 251) {
            distanceX = (50 * (a - 231));
        }
        else if(a > 251 && a <= 272) {
            distanceX = (50 * (a - 252));
        }
        else if(a > 272 && a <= 293) {
            distanceX = (50 * (a - 273));
        }
        else if(a > 293 && a <= 314) {
            distanceX = (50 * (a - 294));
        }

        //....CALCULLAR EIXO Y....//
        if(a <= 20) {
            distanceY = 0;
        }
        else if(a > 20 && a <= 41) {
            distanceY = 50;
        }
        else if(a > 41 && a <= 62) {
            distanceY = 100;
        }
        else if(a > 62 && a <= 83) {
            distanceY = 150;
        }
        else if(a > 83 && a <= 104) {
            distanceY = 200;
        }
        else if(a > 104 && a <= 125) {
            distanceY = 250;
        }
        else if(a > 125 && a <= 146) {
            distanceY = 300;
        }
        else if(a > 146 && a <= 167) {
            distanceY = 350;
        }
        else if(a > 167 && a <= 188) {
            distanceY = 400;
        }
        else if(a > 188 && a <= 209) {
            distanceY = 450;
        }
        else if(a > 209 && a <= 230) {
            distanceY = 500;
        }
        else if(a > 230 && a <= 251) {
            distanceY = 550;
        }
        else if(a > 251 && a <= 272) {
            distanceY = 600;
        }
        else if(a > 272 && a <= 293) {
            distanceY = 650;
        }
        else if(a > 293 && a <= 314) {
            distanceY = 700;
        }

        if (mapSplitComplete[a] == "W") {
            lego += '<div class="wall" style="top:' + distanceY + 'px; left:' + distanceX + 'px"></div> ';
            wall_X [a] = distanceX;
            wall_Y [a] = distanceY;
        }
        else if(mapSplitComplete[a] == " ") {
            lego += '<div class="path" style="top:' + distanceY + 'px; left:' + distanceX + 'px"></div> ';
        }
        else if(mapSplitComplete[a] == "R") {
            lego += '<div id="begin" style="top:' + distanceY + 'px; left:' + distanceX + 'px"><div id="caracter"></div></div> ';
        }
        else if(mapSplitComplete[a] == "F") {
            lego += '<div id="end" style="top:' + distanceY + 'px; left:' + distanceX + 'px"></div> ';
        }
    }
    return labirinth.innerHTML = lego;
}

createLabirinth()

/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
////////////////////////////COLISÃO//////////////////////////////////
/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
let wall = document.getElementsByClassName("wall");
let caracter = document.getElementById('caracter');
let permision = true;

let caracterTop = 450;
let caracterLeft = 0;


function avanteNegX() {
    for(a=0; a<=314;a++) {
        let sumWallXCaracterX = wall_X[a] - caracterLeft;
        let sumWallYCaracterY = wall_Y[a] - caracterTop;
        if(sumWallXCaracterX == 0 && sumWallYCaracterY == 0) {
            permision = false;
            caracterLeft += -50;
        }
    } 
    return permision
}

function avanteX() {
    for(a=0; a<=314;a++) {
        let sumWallXCaracterX = wall_X[a] - caracterLeft;
        let sumWallYCaracterY = wall_Y[a] - caracterTop;
        if(sumWallXCaracterX == 0 && sumWallYCaracterY == 0) {
            permision = false;
            caracterLeft += 50;
        }
    } 
    return permision
}

function avanteNegY() {
    for(a=0; a<=314;a++) {
        let sumWallXCaracterX = wall_X[a] - caracterLeft;
        let sumWallYCaracterY = wall_Y[a] - caracterTop;
        if(sumWallXCaracterX == 0 && sumWallYCaracterY == 0) {
            permision = false;
            caracterTop += -50;
        }
    } 
    return permision
}

function avanteY() {
    for(a=0; a<=314;a++) {
        let sumWallXCaracterX = wall_X[a] - caracterLeft;
        let sumWallYCaracterY = wall_Y[a] - caracterTop;
        if(sumWallXCaracterX == 0 && sumWallYCaracterY == 0) {
            permision = false;
            caracterTop += 50;
        }
    } 
    return permision
}

function outOfLimits() {
    if(caracterLeft < 0) {
        permision = false;
        caracterLeft += 50;
    }
    return permision
}

function outOfLimits2() {
    if(caracterLeft > 1000) {
        permision = false;
        caracterLeft += -50;
    }
    return permision
}

/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
////////////////////////////VITORIA//////////////////////////////////
/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////

function victory() {
    if(caracterTop == 400 && caracterLeft == 1000) {
        alert("VOCÊ VENCEU!")
    }
}


/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
///////////////////////////PERSONAGEM////////////////////////////////
/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////

// 37=esquerda  -  38=cima  -  39=direita  -  40=baixo
document.addEventListener('keydown', (event) => {
    const keyName = event.keyCode;
    if (keyName === 40) {
        caracterTop += 50;
        avanteNegY()
        if(permision == true) {
            caracter.style.top = caracterTop + 'px';
        }
        permision = true;
    }

    else if (keyName === 38) {
        caracterTop += -50;
        avanteY()
        if(permision == true) {
            caracter.style.top = caracterTop + 'px';
        }
        permision = true;
    }

    else if (keyName ===37) {
        caracterLeft += -50;
        avanteX()
        outOfLimits()
        victory()
        if(permision == true) {
        caracter.style.left = caracterLeft + 'px';
        }
        permision = true;
    }
    else if (keyName === 39) {
        caracterLeft += 50;
        avanteNegX()
        outOfLimits2()
        victory()
        if(permision == true) {
            caracter.style.left = caracterLeft + 'px';
            }
            permision = true;
    }

  });